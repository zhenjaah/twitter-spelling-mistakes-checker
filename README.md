# README #

Welcome to this repository.

### What is this repository for? ###

In this repository all files needed for reproduction/replication of the results of 
"Twitter spelling mistakes: 2012 and 2018 compared" are included.

### How do I get set up? ###

After cloning this repository to your own machine, all you have to do is execute the bash file "tweets_filterer.sh" in both the 2012
and 2018 folder. Note: it might take some time for the script to finish.
The next step is execution the count_spelling_mistakes.sh script in both the folders.
This script will return exactly the amount of spelling mistakes in the tweets.