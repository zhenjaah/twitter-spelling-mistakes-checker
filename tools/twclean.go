package main

import (
	"fmt"
	"github.com/pebbe/util"
	"io"
	"os"
	"regexp"
	"strings"
)

var (
	reWord = regexp.MustCompile("(" +
		// url
		"[hH][tT][tT][pP][sS]?:([-A-Za-z0-9\\._~:/?#\\[\\]@!$&'\\(\\)\\*\\+,;=]|%[0-9a-fA-f][0-9a-fA-f])*" +
		"|" +
		// hashtag
		"#[\\p{L}0-9]+" +
		"|" +
		// mention
		"@[a-zA-Z0-9_]+" +
		"|" +
		// word
		"\\p{L}(-\\p{L}|\\p{L})+" +
		")")

	doRt   = false
	doUrl  = false
	doAt   = false
	doHash = false
)

func main() {
	if util.IsTerminal(os.Stdin) {
		usage()
		return
	}
	for _, arg := range os.Args[1:] {
		for _, c := range arg {
			if c == 'r' {
				doRt = true
			} else if c == 'u' {
				doUrl = true
			} else if c == 'a' {
				doAt = true
			} else if c == 'h' {
				doHash = true
			} else {
				usage()
				return
			}
		}
	}
	if !doRt && !doUrl && !doAt && !doHash {
		doRt = true
		doUrl = true
		doAt = true
		doHash = true
	}
	r := util.NewReaderSize(os.Stdin, 50000)
	for  {
		line, err := r.ReadLineString()
		if err == io.EOF {
			break
		}
		util.CheckErr(err)

		words := strings.Fields(reWord.ReplaceAllString(line, " $1 "))
		w := []string{}
		for _, ww := range words {
			if !(doRt && ww == "RT" ||
				doAt && strings.HasPrefix(ww, "@") ||
				doHash && strings.HasPrefix(ww, "#") ||
				doUrl && strings.HasPrefix(ww, "http:") ||
				doUrl && strings.HasPrefix(ww, "https:")) {
				w = append(w, ww)
			}
		}
		fmt.Println(strings.Join(w, " "))
	}
}

func usage() {
	fmt.Printf(`
Usage: %s options < tweet(s)

Purpose: filter tweets before submitting to textcat

Options:

    r = skip RT
    a = skip @-mentions
    h = skip hashtags
    u = skip URLs

If no options are given, skip all these things

`, os.Args[0])
}
