#!/bin/bash

echo "This is how many times 'foto's' has been misspelled as 'fotos':"
cat tweets_filtered_final_2018.txt |
grep -wc "fotos"

echo "This is how many times 'auto's' has been misspelled as 'autos':"
cat tweets_filtered_final_2018.txt |
grep -wc "autos"

echo "This is how many times 'ideeën' has been misspelled as 'ideeen':"
cat tweets_filtered_final_2018.txt |
grep -wc "ideeen"

echo "This is how many times 'e-mail' has been misspelled as 'email':"
cat tweets_filtered_final_2018.txt |
grep -wc "email"

echo "This is how many times 'cadeaus' has been misspelled as 'cadeau's' or 'kado':"
cat tweets_filtered_final_2018.txt |
grep -wc "cadeau's\|kado"

echo "This is how many times 'knieën' has been misspelled as 'knieen':"
cat tweets_filtered_final_2018.txt |
grep -wc "knieen"

echo "This is how many times some of the common nationalities have been written without a captial letter (and therefore are a mistake):"
cat tweets_filtered_final_2018.txt |
grep -wc "nederlandse\|engelse\|duitse\|franse\|amerikaanse"

echo "This is how many times 'beter dan' and 'groter dan' have been misspelled as 'beter als' and 'groter als':"
cat tweets_filtered_final_2018.txt |
grep -wc "groter als\|beter als"

echo "This is how many times 'dezelfde' and 'hetzelfde' have been misspelled as 'de zelfde' and 'het zelfde':"
cat tweets_filtered_final_2018.txt |
grep -wc "het zelfde\|de zelfde"

echo "This is how many times 'chic' has been misspelled as 'chique':"
cat tweets_filtered_final_2018.txt |
grep -wc "chique"

echo "The total amount of mistakes made: 335."
echo "The percentage of mistakes made: 335 / 166796 * 100 = 0,20%"
