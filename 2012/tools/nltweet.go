package main

import (
	"Local/tweetlang"
	"github.com/pebbe/textcat"
	"github.com/pebbe/util"

	"bufio"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strings"
)

var (
	reWord = regexp.MustCompile("(" +
		// url
		"[hH][tT][tT][pP][sS]?:([-A-Za-z0-9\\._~:/?#\\[\\]@!$&'\\(\\)\\*\\+,;=]|%[0-9a-fA-f][0-9a-fA-f])*[^- \t\n\r\f!\"#$%&'()*+,./@:;<=>\\[\\\\\\]^_`{|}~]" +
		"|" +
		// hashtag
		"#[\\p{L}0-9]+" +
		"|" +
		// mention
		"@[a-zA-Z0-9_]+" +
		"|" +
		// word
		"\\p{L}(-\\p{L}|\\p{L})+" +
		")")

	opt_i = flag.Int("i", 0, "field index")
	opt_e = flag.Bool("e", false, "full check")
)

func syntax() {
	fmt.Printf(`
Usage: %s [-e] [-i int] < tweets

Filter only Dutch tweets.

Options:

    -e     : use textcat and naive bayes
             default: naive bayes only

    -i int : index of field to test
             indexing starts with 1
             fields are separated by tab

Without option -i, the complete line is tested

`, os.Args[0])
}

func main() {
	flag.Usage = syntax
	flag.Parse()
	if util.IsTerminal(os.Stdin) {
		syntax()
		return
	}

	tc := textcat.NewTextCat()
	tc.EnableAllUtf8Languages()
	tc.DisableLanguages("af.utf8", "fy.utf8")
	tc.SetMinDocSize(10) // dat is *erg* kort (default is 25)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()

		key := text
		if *opt_i > 0 {
			key = strings.Split(text, "\t")[*opt_i-1]
		}

		if *opt_e {

			// tokenizer
			words := strings.Fields(reWord.ReplaceAllString(key, " $1 "))

			// taalrader
			w := []string{}
			for _, ww := range words {
				if !(ww == "RT" ||
					strings.HasPrefix(ww, "@") ||
					strings.HasPrefix(ww, "#") ||
					strings.HasPrefix(ww, "http:") ||
					strings.HasPrefix(ww, "https:")) {
					w = append(w, ww)
				}
			}
			languages, err := tc.Classify(strings.Join(w, " "))
			if err != nil {
				continue
			}

			if !nl_in_lang(languages) {
				continue
			}

		}

		// wat overblijft testen met tweetlang
		if tweetlang.IsNL(key) {
			fmt.Println(text)
		}
	}
	util.CheckErr(scanner.Err())
}

func url(u string) (string, bool) {
	parts := strings.Split(u, "/")
	if len(parts) < 3 || parts[2] == "t.co" {
		return "", false
	}
	parts2 := strings.Split(parts[2], ".")
	if len(parts2) < 2 || len(parts2[len(parts2)-1]) < 2 {
		return "", false
	}
	return "." + parts2[len(parts2)-1], true
}

func nl_in_lang(languages []string) bool {
	for _, l := range languages {
		if l == "nl.utf8" {
			return true
		}
	}
	return false
}
