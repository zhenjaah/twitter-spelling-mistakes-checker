
Hier vind je enkele hulpprogramma's voor het werken met tweets.

Om deze programma's te gebruiken, en om de voorbeelden die hieronder
volgen te doen, zet deze directory in je PATH:

    /net/aps/haytabo/bin


nltweets
========

Dit programma filtert Nederlandstalige tweets.

Voorbeelden:

Voor tweets die al voorgefilterd zijn met textcat:

    zcat ../Tweets/Tekst/2013/12/20131201\:00.out.gz | nltweet -i 2

Voor tweets die nog niet voorgefilterd zijn:

    zcat ../Tweets/Tekst/2013/12/20131201\:00.out.gz | nltweet -i 2 -e


twclean
=======

Dit is om bepaalde elementen uit tweets te halen zodat "schone" tekst
overblijft, die je kunt categoriseren met textcat.

Voorbeeld:

    twclean < ../Samples/2012/12/20121201\:00.txt | textcat -l

Voor overzicht van opties, run twclean zonder argumenten.


tweet2tab
=========

Leest tweets in als JSON-object (1 per regel), haalt daar de gewenste
gegevens uit, en print die als een tabel met tabs tussen de velden.

Voorbeeld:

    gunzip -c ../Tweets/2012/12/20121201\:01.out.gz | tweet2tab -i user text

Voor overzicht van opties, run tweet2tab zonder argumenten.

streamer
========

Dit werkt alleen op `cat /net/aistaff/alfa/server` en op
`cat /net/aistaff/alfa/server-www`, omdat de poort voor andere hosts dicht is.


Dit programma laat je live zien welke Nederlandstalige tweets er binnenkomen.

Voorbeeld:

    streamer
