package main

import (
	"github.com/pebbe/util"
	zmq "github.com/pebbe/zmq4"

	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

const (
	remote = "tcp://%s:"
	local  = "tcp://localhost:"
	port   = "44557"
)

func main() {

	b, err := ioutil.ReadFile("/net/aistaff/alfa/server")
	util.CheckErr(err)
	machine := strings.TrimSpace(string(b))
	b, err = ioutil.ReadFile("/net/aistaff/alfa/servername")
	util.CheckErr(err)
	machinename := strings.TrimSpace(string(b))

	server := fmt.Sprintf(remote, machine) + port
	host, _ := os.Hostname()
	if strings.HasPrefix(host, machinename) {
		server = local + port
	}

	subscriber, err := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close() // cancel subscription before exit
	util.CheckErr(err)
	util.CheckErr(subscriber.Connect(server))

	util.CheckErr(subscriber.SetSubscribe("NL"))

	// Without signal handling, Go will exit on signal, even if the signal was caught by ZeroMQ
	chSignal := make(chan os.Signal, 1)
	signal.Notify(chSignal, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)

LOOP:
	for {
		msg, err := subscriber.Recv(0)
		if err != nil {
			log.Println(err)
			break
		}
		a := strings.Split(msg, "\t")
		end := len(a) - 1
		fmt.Println(a[end])
		select {
		case sig := <-chSignal:
			// signal was caught by Go
			log.Println("Signal:", sig)
			break LOOP
		default:
		}
	}
}
